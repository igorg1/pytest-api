import pytest
import time

from ..url_functions import zones_get
from ..url_functions import zones_zone_get
from ..url_functions import zones_zone_params_get
from ..url_functions import zones_zone_pictures_get

from ..url_functions import config_mode_post
from ..url_functions import zones_post
from ..url_functions import zones_zone_pictures_post

from ..url_functions import zones_zone_put
from ..url_functions import zones_zone_params_put

from ..url_functions import zones_zone_patch

from ..url_functions import config_mode_delete
from ..url_functions import zones_zone_delete

ZONE_IDS = []


@pytest.mark.run(order=2)
def test_enter_installer_mode():
    config_mode_post()


@pytest.mark.run(order=0)
def test_get_zones_list():
    global ZONE_IDS
    zones_get(zone_ids=ZONE_IDS)
    print 'ZONES:', ZONE_IDS


@pytest.mark.run(order=1)
def test_get_zone_detail():
    zones_zone_get(ZONE_IDS[-1])


# @pytest.mark.skip(reason='not relevant')
@pytest.mark.run(order=3)
def test_add_zone():
    global ZONE_IDS
    zone_id = zones_post()
    ZONE_IDS.append(zone_id)
    ZONE_IDS.sort()
    print 'ZONES after add:', ZONE_IDS


# @pytest.mark.skip(reason='not relevant')
# @pytest.mark.xfail
@pytest.mark.run(order=5)
def test_delete_zone():
    global ZONE_IDS
    zones_zone_delete(ZONE_IDS[-1])
    ZONE_IDS.pop()


@pytest.mark.run(order=4)
def test_put_zone():
    zones_zone_put(ZONE_IDS[-1])


@pytest.mark.run(order=4)
def test_get_zone_params():
    zones_zone_params_get(ZONE_IDS[0])


# @pytest.mark.skip(reason='not relevant')
@pytest.mark.run(order=6)
def test_put_zone_params():
    zones_zone_params_put(ZONE_IDS[-1])


@pytest.mark.run(order=7)
def test_get_zone_pictures():
    zones_zone_pictures_get(ZONE_IDS[-1])


# @pytest.mark.skip(reason='does not work with my Cam')
@pytest.mark.xfail
def test_post_zone_pictures():
    zones_zone_pictures_post(ZONE_IDS[-1])


@pytest.mark.run(order=11)
def test_exit_installer_mode():
    config_mode_delete()


@pytest.mark.xfail
# @pytest.mark.skip(reason='fails on timeout')
def test_zone_bypass_do():
    zones_zone_patch(ZONE_IDS[-1], bypass=True)
    # time.sleep(15)


@pytest.mark.xfail
# @pytest.mark.skip(reason='fails on timeout')
def test_zone_bypass_cancel():
    zones_zone_patch(ZONE_IDS[-1], bypass=False)
    # time.sleep(5)


@pytest.mark.run(order=12)
def test_print_zones():
    print 'ZONES:', ZONE_IDS
