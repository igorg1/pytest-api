import pytest
import time

from ..url_functions import users_get
from ..url_functions import users_user_get

from ..url_functions import config_mode_post

from ..url_functions import users_user_put
from ..url_functions import login

from ..url_functions import config_mode_delete

USER_IDS = []


# @pytest.mark.skip(reason='not relevant')
@pytest.mark.run(order=2)
def test_enter_installer_mode():
    config_mode_post()


@pytest.mark.run(order=0)
def test_get_users_list():
    global USER_IDS
    users_get(user_ids=USER_IDS)
    print 'USERS:', USER_IDS


@pytest.mark.run(order=3)
def test_user_login():
    login()


@pytest.mark.run(order=1)
def test_get_user_detail():
    users_user_get(USER_IDS[0])


@pytest.mark.run(order=4)
def test_put_user_detail():
    users_user_put(USER_IDS[0])


@pytest.mark.run(order=10)
def test_exit_installer_mode():
    config_mode_delete()


@pytest.mark.run(order=11)
def test_print_users():
    print 'USERSSSSS:', USER_IDS
