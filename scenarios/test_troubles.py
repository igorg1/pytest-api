import pytest
import time

from ..url_functions import troubles_get
from ..url_functions import troubles_delete


@pytest.mark.run(order=0)
def test_get_troubles():
    troubles_get()


# @pytest.mark.skip(reason='not relevant')
@pytest.mark.run(order=1)
def test_delete_troubles():
    troubles_delete()


# @pytest.mark.skip(reason='not relevant')
@pytest.mark.run(order=2)
def test_get_troubles_after_del():
    troubles_get()







