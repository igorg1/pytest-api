import pytest
import time

from ..url_functions import outputs_get
from ..url_functions import outputs_output_get

from ..url_functions import config_mode_post
from ..url_functions import outputs_post

from ..url_functions import outputs_output_put

from ..url_functions import outputs_output_patch

from ..url_functions import config_mode_delete
from ..url_functions import outputs_output_delete

OUTPUT_IDS = []


@pytest.mark.run(order=2)
def test_enter_installer_mode():
    config_mode_post()


@pytest.mark.run(order=0)
def test_get_outputs_list():
    global OUTPUT_IDS
    outputs_get(output_ids=OUTPUT_IDS)
    print 'OUTPUTS:', OUTPUT_IDS


@pytest.mark.run(order=1)
def test_get_output_detail():
    outputs_output_get(OUTPUT_IDS[-1])


@pytest.mark.run(order=3)
def test_add_output():
    global OUTPUT_IDS
    output_id = outputs_post()
    OUTPUT_IDS.append(output_id)
    print 'OUTPUTS after add:', OUTPUT_IDS


# # @pytest.mark.xfail
@pytest.mark.run(order=7)
def test_delete_output():
    global OUTPUT_IDS
    outputs_output_delete(OUTPUT_IDS[-1])
    OUTPUT_IDS.pop()


@pytest.mark.run(order=4)
def test_put_output():
    outputs_output_put(OUTPUT_IDS[-1])


@pytest.mark.run(order=8)
def test_exit_installer_mode():
    config_mode_delete()


# @pytest.mark.run(order=5)
# # @pytest.mark.skip(reason='not relevant')
@pytest.mark.xfail
def test_output_state_off():
    outputs_output_patch(OUTPUT_IDS[0], state=False)
    # time.sleep(1)


# @pytest.mark.run(order=6)
# @pytest.mark.skip(reason='not relevant')
@pytest.mark.xfail
def test_output_state_on():
    outputs_output_patch(OUTPUT_IDS[0], state=True)
    # time.sleep(15)


# @pytest.mark.run(order=3)
@pytest.mark.skip(reason='not relevant')
def test_output_state_on2():
    outputs_output_patch(OUTPUT_IDS[-1], state=True)
    # time.sleep(1)


@pytest.mark.run(order=9)
def test_print_outputs():
    print 'OUTPUTS:', OUTPUT_IDS
