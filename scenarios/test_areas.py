import pytest
import time

from ..url_functions import areas_get
from ..url_functions import areas_area_get
from ..url_functions import events_get

from ..url_functions import config_mode_post

from ..url_functions import areas_area_put

from ..url_functions import areas_area_patch

from ..url_functions import config_mode_delete

AREA_IDS = []


@pytest.mark.run(order=0)
def test_get_areas_list():
    global AREA_IDS
    areas_get(AREA_IDS)
    print 'AREA_IDS:', AREA_IDS


@pytest.mark.run(order=1)
def test_get_area_details():
    areas_area_get(AREA_IDS[0])


@pytest.mark.run(order=2)
def test_enter_installer_mode():
    config_mode_post()


@pytest.mark.run(order=3)
def test_put_area():
    areas_area_put(AREA_IDS[0])


@pytest.mark.run(order=4)
def test_area_arm():
    areas_area_patch(area_id=0, state='arm', force=True)


# @pytest.mark.skip(reason='not relevant')
@pytest.mark.run(order=5)
def test_area_disarm():
    # time.sleep(2)
    areas_area_patch(area_id=0, state='disarm', force=False)


@pytest.mark.run(order=6)
def test_get_events():
    events_get()


@pytest.mark.run(order=7)
def test_exit_installer_mode():
    config_mode_delete()






