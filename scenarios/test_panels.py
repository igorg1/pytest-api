import pytest
# from pytest_regtest import regtest

from ..url_functions.get_url_functions import panels_get
from ..url_functions.get_url_functions import panels_panel_get
from ..url_functions.get_url_functions import pictures_get
from ..url_functions.get_url_functions import events_get

from ..url_functions.post_url_functions import pictures_post

PANEL_IDS = []


# def test_get_panels_list(regtest):
# def test_get_panels_list(regtest_redirect):

@pytest.mark.run(order=0)
def test_get_panels_list():
    global PANEL_IDS
    panels_get(PANEL_IDS)
    print 'Panels ', PANEL_IDS
    # regtest.write('OK')
    # with regtest_redirect():
    #     print 'done1'


@pytest.mark.run(order=1)
def test_get_panel():
    panels_panel_get(PANEL_IDS[0])


@pytest.mark.run(order=2)
def test_get_events():
    events_get()


@pytest.mark.run(order=3)
def test_get_pictures():
    pictures_get()


@pytest.mark.skip(reason='does not work with my Cam')
def test_post_pictures():
    pictures_post()


