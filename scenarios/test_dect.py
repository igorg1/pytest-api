import pytest
import time

from ..url_functions import dect_list_get
from ..url_functions import dect_get
from ..url_functions import dect_measurements_latest_get

from ..url_functions import config_mode_post
from ..url_functions import dect_post

from ..url_functions import config_mode_delete
from ..url_functions import dect_list_delete
from ..url_functions import dect_delete

DECT_IDS = []


# @pytest.mark.skip(reason='not relevant')
@pytest.mark.run(order=3)
def test_enter_installer_mode():
    config_mode_post()


@pytest.mark.run(order=0)
def test_get_dect_list():
    global DECT_IDS
    dect_list_get(dect_ids=DECT_IDS)
    print 'DECTs:', DECT_IDS


# @pytest.mark.skip(reason='not relevant')
@pytest.mark.run(order=1)
def test_get_dect_detail():
    dect_get(DECT_IDS[0])


@pytest.mark.skip(reason='not relevant')
# @pytest.mark.run(order=7)
def test_delete_dect_list():
    dect_list_delete()


@pytest.mark.skip(reason='not relevant')
# @pytest.mark.run(order=6)
def test_delete_dect():
    dect_delete(DECT_IDS[-1])


@pytest.mark.run(order=5)
def test_print_dect():
    print 'DECTs:', DECT_IDS


@pytest.mark.skip(reason='not relevant')
# @pytest.mark.run(order=4)
def test_post_dect():
    dect_post()


@pytest.mark.run(order=8)
def test_exit_installer_mode():
    config_mode_delete()


@pytest.mark.run(order=2)
def test_get_dect_latest_measurements():
    dect_measurements_latest_get()


# @pytest.mark.run(order=9)
# def test_print_dect2():
#     print 'DECTs:', DECT_IDS
