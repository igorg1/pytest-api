"""__author__ = 'Igorg'
"""

import sys

import requests

from ..common_stuff import PANEL_ID
from ..common_stuff import common_check
from ..login import TOKEN
from ..login import URL_BASE


##############################
# #         Panels         # #
##############################

def panels_get(panel_ids):
    print '\n[' + sys._getframe().f_code.co_name + ']'
    url = URL_BASE + '/panels/'
    resp = requests.get(url, headers={'Authorization': 'Bearer ' + TOKEN})
    rc = common_check(resp)
    assert rc is True

    j_cont = resp.json()
    print 'Panels count:', j_cont['count']
    results = j_cont['results']
    try:
        for result in results:
            print 'name:', result['name']
            panel_id = result['id']
            panel_ids.append(panel_id)
            print 'id:', panel_id
            print 'state', result['state'], '\n'
    except KeyError, e:
        print e
        rc = False
    finally:
        assert rc is True


def panels_panel_get(id):
    print '\n[' + sys._getframe().f_code.co_name + ']'
    url = URL_BASE + '/panels/{}/'.format(id)
    resp = requests.get(url, headers={'Authorization': 'Bearer ' + TOKEN})
    rc = common_check(resp)
    assert rc is True

    j_cont = resp.json()
    try:
            print 'name:', j_cont['name']
            print 'id:', j_cont['id']
            print 'state', j_cont['state'], '\n'
    except KeyError, e:
        print e
        rc = False
    finally:
        assert rc is True


def pictures_get():
    print '\n[' + sys._getframe().f_code.co_name + ' on panel {}]'.format(PANEL_ID)
    url = URL_BASE + '/panels/{}/pictures/'.format(PANEL_ID)
    resp = requests.get(url, headers={'Authorization': 'Bearer ' + TOKEN})
    rc = common_check(resp)
    assert rc is True

    j_cont = resp.json()
    print 'count:', j_cont['count']
    print 'next:', j_cont['next']
    print 'previous:', j_cont['previous']

    for idx, result in enumerate(j_cont['results']):
        if idx >= 5:
            break
        print 'control_panel:', result['control_panel']
        assert PANEL_ID == result['control_panel']
        print 'id:', result['id']
        print 'created:', result['created']
        print 'panel_time:', result['panel_time']
        print 'url:', result['url']
        print 'zone:', result['zone']
        print 'zone_name:', result['zone_name']
        print 'event:', result['event']
        print 'key:', result['key']
        print 'type:', result['type']
        print 'pic_index:', result['pic_index']
        print 'pic_total:', result['pic_total']


##############################
# #           Areas        # #
##############################

def areas_get(area_ids):
    print '\n[' + sys._getframe().f_code.co_name + ' on panel {}]'.format(PANEL_ID)
    url = URL_BASE + '/panels/{}/areas/'.format(PANEL_ID)
    resp = requests.get(url, headers={'Authorization': 'Bearer ' + TOKEN})
    rc = common_check(resp)
    assert rc is True

    j_cont = resp.json()
    try:
        for area in j_cont:
            print 'name:', area['name']
            print 'id:', area['id']
            area_ids.append(area['id'])
            print 'exit_delay:', area['exit_delay']
            print 'stay_exit_delay:', area['stay_exit_delay']
            print 'state:', area['state']
            print 'ready_to_arm:', area['ready_to_arm']
            print 'ready_to_stay:', area['ready_to_stay']
            print 'zone_alarm:', area['zone_alarm']
            print 'zone24H alarm:', area['zone24H_alarm']

            # assert area['state'] == 'disarmed'
    except KeyError, e:
        print e
        rc = False
    finally:
        assert rc is True


def areas_area_get(area_id):
    print '\n[' + sys._getframe().f_code.co_name + ' on panel {}'.format(PANEL_ID) + '] id:{}'.format(area_id)
    url = URL_BASE + '/panels/{}/areas/{}/'.format(PANEL_ID, area_id)
    resp = requests.get(url, headers={'Authorization': 'Bearer ' + TOKEN})
    rc = common_check(resp)
    assert rc is True

    j_cont = resp.json()
    try:
        print 'name:', j_cont['name']
        print 'id:', j_cont['id']
        print 'exit_delay:', j_cont['exit_delay']
        print 'stay_exit_delay:', j_cont['stay_exit_delay']
        print 'state:', j_cont['state']
        print 'ready_to_arm:', j_cont['ready_to_arm']
        print 'ready_to_stay:', j_cont['ready_to_stay']
        print 'zone_alarm:', j_cont['zone_alarm']
        print 'zone24H alarm:', j_cont['zone24H_alarm']
    except KeyError, e:
        print e
        rc = False
    finally:
        assert rc is True


##############################
# #           Zones        # #
##############################

def zones_get(zone_ids):
    print '\n[' + sys._getframe().f_code.co_name + ' on panel {}]'.format(PANEL_ID)
    url = URL_BASE + '/panels/{}/zones/'.format(PANEL_ID)
    resp = requests.get(url, headers={'Authorization': 'Bearer ' + TOKEN})
    rc = common_check(resp)
    assert rc is True

    j_cont = resp.json()
    try:
        print 'zones {}'.format(len(j_cont))
        for zone in j_cont:
            print 'id:', zone['id']
            zone_ids.append(zone['id'])
            print 'flags:', zone.get('flags', None)
            # print 'device:', zone['device']
            print 'name:', zone['name']
            print 'areas:', zone['areas']
            print 'hardware_version:', zone.get('hardware_version', None)
            print 'software_version:', zone.get('software_version', None)
            print 'rssi:', zone.get('rssi', 0)
            print 'type:', zone['type']
            print 'id:', zone['id']
            print 'device id:', zone.get('device_id', None)
            print 'dect_id:{ipud:', zone['dect_id'].get('ipud', ''), 'unit_id:', zone['dect_id'].get('unit_id', 0), '}'
            print 'work_mode id:', zone.get('work_mode', None)
            print 'stay_mode_zone:', zone.get('stay_mode_zone', None)

    except KeyError, e:
        print e
        rc = False
    finally:
        assert rc is True


def zones_zone_get(zone_id):
    print '\n[' + sys._getframe().f_code.co_name + ' on panel {}] id:{}'.format(PANEL_ID, zone_id)
    url = URL_BASE + '/panels/{}/zones/{}/'.format(PANEL_ID, zone_id)
    resp = requests.get(url, headers={'Authorization': 'Bearer ' + TOKEN})
    rc = common_check(resp)
    assert rc is True

    j_cont = resp.json()
    try:
        print 'flags:', j_cont.get('flags', None)
        # print 'device:', j_cont['device']
        print 'name:', j_cont['name']
        print 'areas:', j_cont['areas']
        print 'hardware_version:', j_cont.get('hardware_version', None)
        print 'software_version:', j_cont.get('software_version', None)
        print 'rssi:', j_cont.get('rssi', 0)
        print 'type:', j_cont['type']
        print 'id:', j_cont['id']
        print 'device id:', j_cont.get('device_id', None)
        print 'dect_id:{ipud:', j_cont['dect_id'].get('ipud', ''), 'unit_id:', j_cont['dect_id'].get('unit_id', 0), '}'
        print 'work_mode id:', j_cont.get('work_mode', None)
        print 'stay_mode_zone:', j_cont.get('stay_mode_zone', None)
    except KeyError, e:
        print e
        rc = False
    finally:
        assert rc is True


def zones_zone_params_get(zone_id):
    print '\n[' + sys._getframe().f_code.co_name + ' on panel {}] id:{}'.format(PANEL_ID, zone_id)
    url = URL_BASE + '/panels/{}/zones/{}/params/'.format(PANEL_ID, zone_id)
    resp = requests.get(url, headers={'Authorization': 'Bearer ' + TOKEN})
    rc = common_check(resp)
    assert rc is True

    j_cont = resp.json()
    for key, val in j_cont.iteritems():
        print '{}: {}'.format(key, val)

    return j_cont

def zones_zone_pictures_get(zone_id):
    print '\n[' + sys._getframe().f_code.co_name + ' on panel {}] id:{}'.format(PANEL_ID, zone_id)
    url = URL_BASE + '/panels/{}/zones/{}/pictures/'.format(PANEL_ID, zone_id)
    resp = requests.get(url, headers={'Authorization': 'Bearer ' + TOKEN})
    rc = common_check(resp)
    assert rc is True

    j_cont = resp.json()
    print 'count:', j_cont['count']
    print 'next:', j_cont['next']
    print 'previous:', j_cont['previous']

    for idx, result in enumerate(j_cont['results']):
        if idx >= 5:
            break
        print 'control_panel:', result['control_panel']
        assert PANEL_ID == result['control_panel']
        print 'id:', result['id']
        print 'created:', result['created']
        print 'panel_time:', result['panel_time']
        print 'url:', result['url']
        print 'zone:', result['zone']
        print 'zone_name:', result['zone_name']
        print 'event:', result['event']
        print 'key:', result['key']
        print 'type:', result['type']
        print 'pic_index:', result['pic_index']
        print 'pic_total:', result['pic_total']


##############################
# #         Outputs        # #
##############################

def outputs_get(output_ids):
    print '\n[' + sys._getframe().f_code.co_name + ' on panel {}]'.format(PANEL_ID)
    url = URL_BASE + '/panels/{}/outputs/'.format(PANEL_ID)
    resp = requests.get(url, headers={'Authorization': 'Bearer ' + TOKEN})
    rc = common_check(resp)
    assert rc is True

    j_cont = resp.json()
    try:
        print 'outputs {}'.format(len(j_cont))
        for output in j_cont:
            print 'id:', output['id']
            output_ids.append(output['id'])
            print 'silent:', output.get('silent', 0)
            print 'rssi:', output.get('rssi', 0)
            print 'name:', output['name']
            print 'chime_mode:', output.get('chime_mode', 0)
            print 'chime_timer:', output.get('chime_timer', 0)
            print 'device id:', output.get('device_id', None)
            print 'type:', output.get('type', 1)
            print 'dect_id:{ipud:', output['dect_id'].get('ipud', ''), 'unit_id:', output['dect_id'].get('unit_id', 0), '}'
    except KeyError, e:
        print e
        rc = False
    finally:
        assert rc is True


def outputs_output_get(output_id):
    print '\n[' + sys._getframe().f_code.co_name + ' on panel {}] id:{}'.format(PANEL_ID, output_id)
    url = URL_BASE + '/panels/{}/outputs/{}/'.format(PANEL_ID, output_id)
    resp = requests.get(url, headers={'Authorization': 'Bearer ' + TOKEN})
    rc = common_check(resp)
    assert rc is True

    j_cont = resp.json()
    try:
        print 'id:', j_cont['id']
        print 'flags:', j_cont.get('flags', None)
        print 'silent:', j_cont.get('silent', 0)
        print 'rssi:', j_cont.get('rssi', 0)
        print 'name:', j_cont['name']
        print 'chime_mode:', j_cont.get('chime_mode', 0)
        print 'chime_timer:', j_cont.get('chime_timer', 0)
        print 'device id:', j_cont.get('device_id', None)
        print 'type:', j_cont.get('type', 1)
        print 'dect_id:{ipud:', j_cont['dect_id'].get('ipud', ''), 'unit_id:', j_cont['dect_id'].get('unit_id', 0), '}'
    except KeyError, e:
        print e
        rc = False
    finally:
        assert rc is True


##############################
# #        Troubles        # #
##############################

def troubles_get():
    print '\n[' + sys._getframe().f_code.co_name + ' on panel {}]'.format(PANEL_ID)
    url = URL_BASE + '/panels/{}/troubles/'.format(PANEL_ID)
    resp = requests.get(url, headers={'Authorization': 'Bearer ' + TOKEN})
    rc = common_check(resp)
    assert rc is True

    j_cont = resp.json()
    for key, val in j_cont.iteritems():
        print '{}:'.format(key), val
    # try:
    #     print 'can_bus_fail:', j_cont['can_bus_fail']
    #     print 'chime_is_off:', j_cont['chime_is_off']
    #     print 'code_attempts_alarm:', j_cont['code_attempts_alarm']
    #     print 'duress_alarm:', j_cont['duress_alarm']
    #     print 'ethernet_fail:', j_cont['ethernet_fail']
    #     print 'fire_alarm:', j_cont['fire_alarm']
    #     print 'fuse_fail:', j_cont['fuse_fail']
    #     print 'panel_tamper:', j_cont['panel_tamper']
    #     print 'gsm_line_fail:', j_cont['gsm_line_fail']
    #     print 'low_battery:', j_cont['low_battery']
    #     print 'mains_fail:', j_cont['mains_fail']
    #     print 'medical_alarm:', j_cont['medical_alarm']
    #     print 'panic_alarm:', j_cont['panic_alarm']
    #     print 'phone_line_fail:', j_cont['phone_line_fail']
    #     print 'real_time_clock_status:', j_cont['real_time_clock_status']
    #     print 'rf_jamming_alarm:', j_cont['rf_jamming_alarm']
    #     print 'gprs_fail:', j_cont['gprs_fail']
    #     print 'sdcard_fail:', j_cont['sdcard_fail']
    #     print 'wifi_fail:', j_cont['wifi_fail']
    #     print 'dect_fail:', j_cont['dect_fail']
    # except KeyError, e:
    #     print e
    #     rc = False
    # finally:
    #     assert rc is True


##############################
# #         Users          # #
##############################

def users_get(user_ids):
    print '\n[' + sys._getframe().f_code.co_name + ' on panel {}]'.format(PANEL_ID)
    url = URL_BASE + '/panels/{}/users/'.format(PANEL_ID)
    resp = requests.get(url, headers={'Authorization': 'Bearer ' + TOKEN})
    rc = common_check(resp)
    assert rc is True

    j_cont = resp.json()
    try:
        print 'users {}'.format(len(j_cont))
        for user in j_cont:
            print 'id:', user['id']
            user_ids.append(user['id'])
            print 'name:', user['name']
            print 'pendant_id:', user['pendant_id']
            print 'arm:', user['arm']
            print 'stay:', user['stay']
            print 'disarm:', user['disarm']
            print 'disarm_stay:', user['disarm_stay']
            print 'pendant_allow:', user['pendant_allow']
            print 'remote:', user['remote']
            print 'areas:', user['areas']
    except KeyError, e:
        print e
        rc = False
    finally:
        assert rc is True


def users_user_get(user_id):
    print '\n[' + sys._getframe().f_code.co_name + ' on panel {}] id:{}'.format(PANEL_ID, user_id)
    url = URL_BASE + '/panels/{}/users/{}/'.format(PANEL_ID, user_id)
    resp = requests.get(url, headers={'Authorization': 'Bearer ' + TOKEN})
    rc = common_check(resp)
    assert rc is True

    j_cont = resp.json()
    try:
        print 'id:', j_cont['id']
        print 'name:', j_cont['name']
        print 'pendant_id:', j_cont['pendant_id']
        print 'arm:', j_cont['arm']
        print 'stay:', j_cont['stay']
        print 'disarm:', j_cont['disarm']
        print 'disarm_stay:', j_cont['disarm_stay']
        print 'pendant_allow:', j_cont['pendant_allow']
        print 'remote:', j_cont['remote']
        print 'areas:', j_cont['areas']
    except KeyError, e:
        print e
        rc = False
    finally:
        assert rc is True


##############################
# #         Events         # #
##############################

def events_get():
    print '\n[' + sys._getframe().f_code.co_name + ' on panel {}]'.format(PANEL_ID)
    url = URL_BASE + '/panels/{}/events/'.format(PANEL_ID)
    resp = requests.get(url, headers={'Authorization': 'Bearer ' + TOKEN})
    rc = common_check(resp)
    assert rc is True

    j_cont = resp.json()
    try:
        results = j_cont['results']
        for result in results:
            print 'control panel:', result['control_panel']
            assert result['control_panel'] == PANEL_ID
            print 'text:', result['text']
            print 'panel time:', result['panel_time']
            print 'zone:', result['zone']
            pictures = result['pictures']
            for picture in pictures:
                print 'picture url:', picture['url']
            print '\n'
    except KeyError, e:
        print e
        rc = False
    finally:
        assert rc is True


##############################
# #          DECT          # #
##############################

def dect_list_get(dect_ids):
    print '\n[' + sys._getframe().f_code.co_name + ' on panel {}]'.format(PANEL_ID)
    url = URL_BASE + '/panels/{}/dect/'.format(PANEL_ID)
    resp = requests.get(url, headers={'Authorization': 'Bearer ' + TOKEN})
    rc = common_check(resp)
    assert rc is True

    j_cont = resp.json()
    try:
        for device in j_cont:
            print 'id:', device['id']
            dect_ids.append(device['id'])
            print 'ipud:', device['ipud']
            print 'units:'
            for unit in device['units']:
                print '\ttype:', unit['type']
                print '\tbase_type:', unit['base_type']
                print '\tconnected:', unit['connected']
                print '\tid:', unit['id']
                print '\tsub_type:', unit['sub_type']
    except KeyError, e:
        print e
        rc = False
    finally:
        assert rc is True


def dect_get(dect_id):
    print '\n[' + sys._getframe().f_code.co_name + ' on panel {}]'.format(PANEL_ID)
    url = URL_BASE + '/panels/{}/dect/{}/'.format(PANEL_ID, dect_id)
    resp = requests.get(url, headers={'Authorization': 'Bearer ' + TOKEN})
    rc = common_check(resp)
    assert rc is True

    j_cont = resp.json()
    try:
            print 'id:', j_cont['id']
            print 'ipud:', j_cont['ipud']
            print 'units:'
            for unit in j_cont['units']:
                print '\ttype:', unit['type']
                print '\tbase_type:', unit['base_type']
                print '\tconnected:', unit['connected']
                print '\tid:', unit['id']
                print '\tsub_type:', unit['sub_type']
    except KeyError, e:
        print e
        rc = False
    finally:
        assert rc is True


def dect_measurements_latest_get():
    print '\n[' + sys._getframe().f_code.co_name + ' on panel {}'.format(PANEL_ID) + ']'
    url = URL_BASE + '/panels/{}/dect/measurements/latest/'.format(PANEL_ID)
    resp = requests.get(url, headers={'Authorization': 'Bearer ' + TOKEN})
    rc = common_check(resp)
    assert rc is True

    j_cont = resp.json()
    try:
        for meas in j_cont:
            print 'panel_time:', meas['_id']['panel_time']
            print 'air pressure:', meas['air_pressure']
            print 'humidity:', meas['humidity']
            print 'temperature:', meas['temperature'], '\n'
    except KeyError, e:
        print e, '\n'
        rc = False
    finally:
        assert rc is True


