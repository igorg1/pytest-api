import requests
import sys

import pytest

from ..common_stuff import PANEL_ID
from ..common_stuff import common_check
from ..login import TOKEN
from ..login import URL_BASE


def config_mode_delete():
    print '\n[' + sys._getframe().f_code.co_name + ']'
    url = URL_BASE + '/panels/{}/config_mode/'.format(PANEL_ID)
    config_data = {}
    resp = requests.delete(url, headers={'Authorization': 'Bearer ' + TOKEN}, data=config_data)
    rc = common_check(resp)
    assert rc is True

    j_con = resp.json()
    try:
        print 'config: ', j_con['config']
    except KeyError, e:
        print e
        rc = False
    finally:
        assert rc is True


##############################
# #           Zones        # #
##############################

def zones_zone_delete(zone_id):
    print '\n[' + sys._getframe().f_code.co_name + '] id:{}'.format(zone_id)
    url = URL_BASE + '/panels/{}/zones/{}/'.format(PANEL_ID, zone_id)
    resp = requests.delete(url, headers={'Authorization': 'Bearer ' + TOKEN}, data={})
    rc = common_check(resp)
    assert rc is True

    j_con = resp.json()
    print 'json:', j_con
    assert j_con == {}


##############################
# #         Outputs        # #
##############################

def outputs_output_delete(output_id):
    print '\n[' + sys._getframe().f_code.co_name + '] id:{}'.format(output_id)
    url = URL_BASE + '/panels/{}/outputs/{}/'.format(PANEL_ID, output_id)
    resp = requests.delete(url, headers={'Authorization': 'Bearer ' + TOKEN}, data={})
    rc = common_check(resp)
    assert rc is True

    j_con = resp.json()
    print 'json:', j_con
    assert j_con == {}


##############################
# #        Troubles        # #
##############################

def troubles_delete():
    print '\n[' + sys._getframe().f_code.co_name + ']'
    url = URL_BASE + '/panels/{}/troubles/'.format(PANEL_ID)
    resp = requests.delete(url, headers={'Authorization': 'Bearer ' + TOKEN}, data={})
    rc = common_check(resp)
    assert rc is True

    j_con = resp.json()
    print 'json:', j_con
    assert j_con == {}


##############################
# #          DECT          # #
##############################

def dect_list_delete():  # not sure it works
    print '\n[' + sys._getframe().f_code.co_name + ' on panel {}]'.format(PANEL_ID)
    url = URL_BASE + '/panels/{}/dect/'.format(PANEL_ID)
    config_data = {}

    resp = requests.delete(url, headers={'Authorization': 'Bearer ' + TOKEN}, json=config_data)
    rc = common_check(resp)
    assert rc is True

    j_cont = resp.json()
    print 'jssson:', j_cont
    try:
        print 'status:', j_cont['status']
    except KeyError, e:
        print e
    finally:
        assert j_cont['status'] == 'canceled'


def dect_delete(dect_id):
    print '\n[' + sys._getframe().f_code.co_name + ' on panel {}]'.format(PANEL_ID)
    url = URL_BASE + '/panels/{}/dect/{}/'.format(PANEL_ID, dect_id)
    config_data = {}

    resp = requests.delete(url, headers={'Authorization': 'Bearer ' + TOKEN}, json=config_data)
    rc = common_check(resp)
    assert rc is True

    j_cont = resp.json()
    print 'json:', j_cont
    try:
        pass
    except KeyError, e:
        print e
    finally:
        assert j_cont == {}


def dect_headset_delete():  # not sure it's right
    print '\n[' + sys._getframe().f_code.co_name + ' on panel {}]'.format(PANEL_ID)
    url = URL_BASE + '/panels/{}/dect/headset/'.format(PANEL_ID)
    config_data = {}

    resp = requests.delete(url, headers={'Authorization': 'Bearer ' + TOKEN}, json=config_data)
    rc = common_check(resp)
    assert rc is True

    j_cont = resp.json()
    print 'json:', j_cont
    try:
        pass
    except KeyError, e:
        print e
    finally:
        assert j_cont == {}
