import requests
import sys
import time

from ..common_stuff import PANEL_ID
from ..common_stuff import common_check
from ..login import TOKEN
from ..login import URL_BASE


##############################
# #           Areas        # #
##############################

def areas_area_patch(area_id, state, force=False):
    print '\n[' + sys._getframe().f_code.co_name + '] id:{}'.format(area_id)
    assert state in ('arm', 'disarm')
    # area_id = 0
    url = URL_BASE + '/panels/{}/areas/{}/'.format(PANEL_ID, area_id)

    config_data = {
        "state": state,
        "force": force
    }

    timeout = 10
    time_start = time.time()
    while time.time() - time_start < timeout:
        resp = requests.patch(url, headers={'Authorization': 'Bearer ' + TOKEN}, json=config_data)
        rc = common_check(resp)
        assert rc is True

        j_cont = resp.json()
        try:
            print 'name:', j_cont['name']
            print 'id:', j_cont['id']
            print 'state:', j_cont['state']
            if 'in progress' in j_cont['state']:
                time.sleep(timeout/10.0)
                continue
            elif 'arm' == state and 'armed' == j_cont['state']:
                break
            elif 'disarm' == state and 'disarmed' == j_cont['state']:
                break
        except KeyError, e:
            print e
            rc = False
        finally:
            assert rc is True


##############################
# #           Zones        # #
##############################

def zones_zone_patch(zone_id, bypass):
    print '\n[' + sys._getframe().f_code.co_name + '] id:{} bypass:{}'.format(zone_id, bypass)
    url = URL_BASE + '/panels/{}/zones/{}/'.format(PANEL_ID, zone_id)
    print url

    config_data = {
        "bypass": bypass
    }

    # time.sleep(1)
    resp = requests.patch(url, headers={'Authorization': 'Bearer ' + TOKEN}, json=config_data)
    # time.sleep(10)
    rc = common_check(resp)
    assert rc is True

    j_cont = resp.json()
    try:
        print 'name', j_cont['name']
        print 'device_id:', j_cont['device_id']
        print 'bypass:', j_cont['bypass']
    except KeyError, e:
        print e
        rc = False
    finally:
        assert rc is True


##############################
# #         Outputs        # #
##############################

def outputs_output_patch(output_id, state):
    print '\n[' + sys._getframe().f_code.co_name + '] id:{} state:{}'.format(output_id, state)
    url = URL_BASE + '/panels/{}/outputs/{}/'.format(PANEL_ID, output_id)
    print url

    config_data = {
        "state": state
    }

    # time.sleep(1)
    resp = requests.patch(url, headers={'Authorization': 'Bearer ' + TOKEN}, json=config_data)
    # time.sleep(10)
    rc = common_check(resp)
    assert rc is True

    j_cont = resp.json()
    try:
        print 'name', j_cont['name']
        print 'device_id:', j_cont['device_id']
        print 'state:', j_cont['state']
    except KeyError, e:
        print e
        rc = False
    finally:
        assert rc is True

