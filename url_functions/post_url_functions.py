import requests
import sys

from ..common_stuff import PANEL_ID
from ..common_stuff import common_check
from ..login import TOKEN
from ..login import URL_BASE
from ..login import USER_NAME


def config_mode_post():
    print '\n[' + sys._getframe().f_code.co_name + ']'
    url = URL_BASE + '/panels/{}/config_mode/'.format(PANEL_ID)
    config_data = {}
    resp = requests.post(url, headers={'Authorization': 'Bearer ' + TOKEN, 'X-Crow-CP-installer': '000000'}, data=config_data)
    rc = common_check(resp)
    assert rc is True

    j_cont = resp.json()
    try:
        print 'config: ', j_cont['config']
    except KeyError, e:
        print e
        rc = False
    finally:
        assert rc is True


def pictures_post():
    print '\n[' + sys._getframe().f_code.co_name + ' on panel {}]'.format(PANEL_ID)
    url = URL_BASE + '/panels/{}/pictures/'.format(PANEL_ID)
    resp = requests.post(url, headers={'Authorization': 'Bearer ' + TOKEN}, json={})
    rc = common_check(resp)
    assert rc is True

    j_cont = resp.json()
    print j_cont  # TODO print specific values


##############################
# #           Zones        # #
##############################

def zones_post():
    print '\n[' + sys._getframe().f_code.co_name + ']'
    url = URL_BASE + '/panels/{}/zones/'.format(PANEL_ID)
    config_data = {
        "name": "Cam new",
        "device_id": 900054,
        # "type": 55,
        "work_mode": 0,
        "stay_mode_zone": True
    }

    resp = requests.post(url, headers={'Authorization': 'Bearer ' + TOKEN}, json=config_data)
    rc = common_check(resp)
    assert rc is True

    j_cont = resp.json()
    try:
        print 'id', j_cont['id']
        print 'device_id:', j_cont['device_id']
        print 'name:', j_cont['name']
        print 'work_mode:', j_cont['work_mode']
        print 'stay_mode_zone:', j_cont['stay_mode_zone']
        print 'type:', j_cont['type']
    except KeyError, e:
        print e
        rc = False
    finally:
        assert rc is True

    return j_cont['id']


def zones_zone_pictures_post(zone_id):
    print '\n[' + sys._getframe().f_code.co_name + ' on panel {}] id:{}'.format(PANEL_ID, zone_id)
    url = URL_BASE + '/panels/{}/zones/{}/pictures/'.format(PANEL_ID, zone_id)
    resp = requests.post(url, headers={'Authorization': 'Bearer ' + TOKEN}, json={})
    # resp = requests.post(url, headers={'Authorization': 'Bearer ' + TOKEN, 'Accept': 'application/octet-stream'}, json={})
    rc = common_check(resp)
    assert rc is True

    j_cont = resp.json()
    print j_cont  # TODO print specific values


##############################
# #         Outputs        # #
##############################

def outputs_post():
    print '\n[' + sys._getframe().f_code.co_name + ']'
    url = URL_BASE + '/panels/{}/outputs/'.format(PANEL_ID)
    config_data = {
        "name": "Output new",
        "device_id": 123456,
        "chime_mode": 1,
        "chime_timer": 50
    }

    resp = requests.post(url, headers={'Authorization': 'Bearer ' + TOKEN}, json=config_data)
    rc = common_check(resp)
    assert rc is True

    j_cont = resp.json()
    try:
        print 'id', j_cont['id']
        print 'device_id:', j_cont['device_id']
        print 'name:', j_cont['name']
        print 'chime_mode:', j_cont['chime_mode']
        print 'chime_timer:', j_cont['chime_timer']
        print 'type:', j_cont['type']
    except KeyError, e:
        print e
        rc = False
    finally:
        assert rc is True

    return j_cont['id']


##############################
# #         Users          # #
##############################

def login():
    print '\n[' + sys._getframe().f_code.co_name + ']'
    url = URL_BASE + '/panels/{}/login/'.format(PANEL_ID)
    config_data = {
        "user": USER_NAME
    }

    resp = requests.post(url, headers={'Authorization': 'Bearer ' + TOKEN}, json=config_data)
    rc = common_check(resp)
    assert rc is True

    j_cont = resp.json()
    assert j_cont["status"] == "success"


##############################
# #          DECT          # #
##############################

def dect_post():  # not sure it's right
    print '\n[' + sys._getframe().f_code.co_name + ' on panel {}]'.format(PANEL_ID)
    url = URL_BASE + '/panels/{}/dect/'.format(PANEL_ID)
    config_data = {
        "units": {
            "type": 231,
            "base_type": 1,
            "connected": False,
            "id": 1,
            "sub_type": 263
        },
        "ipud": "02:7b:a0:0d:83"
        # "id": 1
    }

    resp = requests.post(url, headers={'Authorization': 'Bearer ' + TOKEN}, json={}) #config_data)
    rc = common_check(resp)
    assert rc is True

    j_cont = resp.json()
    try:
        print 'status:', j_cont['status']
    except KeyError, e:
        print e
    finally:
        assert j_cont['status'] == 'learning'


def dect_headset_post():  # not sure it's right
    print '\n[' + sys._getframe().f_code.co_name + ' on panel {}]'.format(PANEL_ID)
    url = URL_BASE + '/panels/{}/dect/headset/'.format(PANEL_ID)
    config_data = {}

    resp = requests.post(url, headers={'Authorization': 'Bearer ' + TOKEN}, json=config_data)
    rc = common_check(resp)
    assert rc is True

    j_cont = resp.json()
    try:
        print 'status:', j_cont['status']
    except KeyError, e:
        print e
    finally:
        assert j_cont['status'] == 'learning'

