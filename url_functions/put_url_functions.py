import requests
import sys

from ..common_stuff import PANEL_ID
from ..common_stuff import common_check
from ..login import TOKEN
from ..login import URL_BASE


##############################
# #           Areas        # #
##############################

def areas_area_put(area_id):
    print '[' + sys._getframe().f_code.co_name + '] id:{}'.format(area_id)
    url = URL_BASE + '/panels/{}/areas/{}/'.format(PANEL_ID, area_id)

    config_data = {
        "name": "Area room 1",  # mandatory field, otherwise 400 - Bad request
        "exit_delay": 3,        # mandatory field, otherwise 400 - Bad request
        "stay_exit_delay": 4    # mandatory field, otherwise 400 - Bad request
    }

    resp = requests.put(url, headers={'Authorization': 'Bearer ' + TOKEN}, json=config_data)  # json.dumps(config_data))
    rc = common_check(resp)
    assert rc is True

    j_cont = resp.json()
    try:
        print 'name', j_cont['name']
        print 'exit_delay:', j_cont['exit_delay']
        print 'stay_exit_delay:', j_cont['stay_exit_delay']
    except KeyError, e:
        print e
        rc = False
    finally:
        assert rc is True


##############################
# #           Zones        # #
##############################

def zones_zone_put(zone_id):
    print '\n[' + sys._getframe().f_code.co_name + '] id:{}'.format(zone_id)
    # zone_id = 9
    url = URL_BASE + '/panels/{}/zones/{}/'.format(PANEL_ID, zone_id)

    config_data = {
        "name": "New 2",        # mandatory field, otherwise 400 - Bad request
        "device_id": 900055,    # mandatory field, otherwise 400 - Bad request
        "work_mode": 1,
        "stay_mode_zone": True
    }

    resp = requests.put(url, headers={'Authorization': 'Bearer ' + TOKEN}, json=config_data)  # json.dumps(config_data))
    rc = common_check(resp)
    assert rc is True

    j_cont = resp.json()
    try:
        print 'name', j_cont['name']
        print 'device_id:', j_cont['device_id']
        print 'stay_mode_zone:', j_cont['stay_mode_zone']
    except KeyError, e:
        print e
        rc = False
    finally:
        assert rc is True


def zones_zone_params_put(zone_id):
    print '[' + sys._getframe().f_code.co_name + '] id:{}'.format(zone_id)
    url = URL_BASE + '/panels/{}/zones/{}/params/'.format(PANEL_ID, zone_id)

    get_resp = requests.get(url, headers={'Authorization': 'Bearer ' + TOKEN})
    rc = common_check(get_resp)
    assert rc is True

    config_data = get_resp.json()
    resp = requests.put(url, headers={'Authorization': 'Bearer ' + TOKEN}, json=config_data)
    rc = common_check(resp)
    assert rc is True

    j_cont = resp.json()
    for key, val in j_cont.iteritems():
        print '{}: {}'.format(key, val)


##############################
# #         Outputs        # #
##############################

def outputs_output_put(output_id):
    print '\n[' + sys._getframe().f_code.co_name + '] id:{}'.format(output_id)
    # zone_id = 9
    url = URL_BASE + '/panels/{}/outputs/{}/'.format(PANEL_ID, output_id)

    config_data = {
        "name": "Output new",   # mandatory field, otherwise 400 - Bad request
        "device_id": 123456,    # mandatory field, otherwise 400 - Bad request
        "chime_mode": 1,
        "chime_timer": 50
    }

    resp = requests.put(url, headers={'Authorization': 'Bearer ' + TOKEN}, json=config_data)  # json.dumps(config_data))
    rc = common_check(resp)
    assert rc is True

    j_cont = resp.json()
    try:
        print 'name', j_cont['name']
        print 'device_id:', j_cont['device_id']
        print 'chime_mode:', j_cont['chime_mode']
        print 'chime_timer:', j_cont['chime_timer']
    except KeyError, e:
        print e
        rc = False
    finally:
        assert rc is True


##############################
# #         Users          # #
##############################

def users_user_put(output_id):
    print '[' + sys._getframe().f_code.co_name + '] id:{}'.format(output_id)
    url = URL_BASE + '/panels/{}/users/{}/'.format(PANEL_ID, output_id)

    get_resp = requests.get(url, headers={'Authorization': 'Bearer ' + TOKEN})
    rc = common_check(get_resp)
    assert rc is True

    config_data = get_resp.json()
    config_data['stay'] = not config_data['stay']
    resp = requests.put(url, headers={'Authorization': 'Bearer ' + TOKEN}, json=config_data)
    rc = common_check(resp)
    assert rc is True

    j_cont = resp.json()
    for key, val in j_cont.iteritems():
        print '{}: {}'.format(key, val)

