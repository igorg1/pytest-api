"""__author__ = 'Igorg'
"""

import sys
import time

import requests

from ..common_stuff import PANEL_ID
from ..common_stuff import DEVICE_TYPES
from ..common_stuff import common_check
from ..login import TOKEN
from ..login import URL_BASE

from get_url_functions import zones_zone_params_get

##############################
# #          Devices       # #
##############################

def devices_get(device_ids):
    print '\n[' + sys._getframe().f_code.co_name + ' on panel {}]'.format(PANEL_ID)
    url = URL_BASE + '/wizard/{}/devices/'.format(PANEL_ID)
    resp = requests.get(url, headers={'Authorization': 'Bearer ' + TOKEN})
    rc = common_check(resp)
    assert rc is True

    j_cont = resp.json()
    try:
        print 'devices {}'.format(len(j_cont))
        for device in j_cont:
            print 'id:', device['id']
            device_ids.append(device['id'])
            print 'name:', device['name']
            print 'device id:', device.get('device_id', None)

    except KeyError, e:
        print e
        rc = False
    finally:
        assert rc is True


def devices_cam_post(wizard_data, refer_params):
    print '\n[' + sys._getframe().f_code.co_name + ']'

    # Check device_id already added devices
    device_id_new = int(wizard_data['code'][wizard_data['code'].rfind('-')+1:])
    url = URL_BASE + '/wizard/{}/devices/'.format(PANEL_ID)
    resp_get = requests.get(url, headers={'Authorization': 'Bearer ' + TOKEN})
    rc = common_check(resp_get)
    assert rc is True
    j_get = resp_get.json()
    for device in j_get:
        if device['device_id'] == device_id_new:
            print 'Device with ID {} is already connected'.format(device_id_new)
            assert False

    # Add device
    resp = requests.post(url, headers={'Authorization': 'Bearer ' + TOKEN}, json=wizard_data)
    rc = common_check(resp)
    assert rc is True

    j_cont = resp.json()
    print 'json:', j_cont
    try:
        print 'device', j_cont['device']
        print 'id', j_cont['id']
        print 'name', j_cont['name']
        print 'device_id', j_cont['device_id']
        print 'stay_mode_zone', j_cont['stay_mode_zone']
        assert j_cont['stay_mode_zone'] == refer_params['stay_mode_zone']
    except KeyError, e:
        print e
        rc = False
    finally:
        assert rc is True

    return j_cont['id']


def devices_acplug_post(wizard_data):
    print '\n[' + sys._getframe().f_code.co_name + ']'

    # Check device_id already added devices
    ipud_new = wizard_data['code'][wizard_data['code'].rfind('-')+1:].lower()
    url = URL_BASE + '/wizard/{}/devices/'.format(PANEL_ID)
    resp_get = requests.get(url, headers={'Authorization': 'Bearer ' + TOKEN})
    rc = common_check(resp_get)
    assert rc is True
    j_get = resp_get.json()
    for device in j_get:
        # if device.get('dect_id', None):
        if device['dect_id'].get('ipud', None):
            if device['dect_id']['ipud'].replace(':', '') == ipud_new:
                print 'Device with IPUD {} is already connected'.format(ipud_new)
                assert False

    # Add device
    resp = requests.post(url, headers={'Authorization': 'Bearer ' + TOKEN}, json=wizard_data)
    rc = common_check(resp)
    assert rc is True

    j_cont = resp.json()
    print 'json:', j_cont
    try:
        print 'id', j_cont['id']
        print 'name', j_cont['name']
        print 'type', j_cont['type']
        print 'ipud', j_cont['dect_id']['ipud']
    except KeyError, e:
        print e
        rc = False
    finally:
        assert rc is True

    return j_cont['id']


##############################
# #         Status         # #
##############################

def status_cam_get(wizard_data):
    print '\n[' + sys._getframe().f_code.co_name + ']'
    device_id_new = int(wizard_data['code'][wizard_data['code'].rfind('-')+1:])
    timeout = 180  # 10 min (10 * 60)  !!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
    time_start = time.time()
    state = ''
    while time.time() - time_start < timeout:
        time.sleep(timeout / 20.0)
        url = URL_BASE + '/wizard/{}/status/'.format(PANEL_ID)
        resp = requests.get(url, headers={'Authorization': 'Bearer ' + TOKEN})
        rc = common_check(resp)
        assert rc is True

        j_cont = resp.json()
        if len(j_cont) == 0:
            timeout /= 10.0
            continue
        try:
            for key, val in j_cont.iteritems():
                if val['config']['device_id'] != device_id_new:
                    continue
                print 'state:', val['state']
                if val['state'] == 0:  # pending
                    state = 'Pending'
                elif val['state'] == 1:  # OK
                    state = 'OK'
                    print 'areas:', val['config']['areas']
                    print 'name:', val['config']['name']
                    print 'device_id:', val['config']['device_id']
                    print 'ts:', val['ts']
                elif val['state'] == 2:  # Failed
                    # rc = False
                    state = 'Failed'
                break

        except KeyError, e:
            print e
            # rc = False
        # finally:
        #     assert rc is True

        if 'OK' in state or 'Failed' in state:
            break

    print 'Time elapsed {} sec. State: {}'.format(time.time() - time_start, state)
    return state


def status_acplug_get(wizard_data):
    print '\n[' + sys._getframe().f_code.co_name + ']'
    ipud_new = wizard_data['code'][wizard_data['code'].rfind('-')+1:].lower()
    timeout = 60  # 10 min (10 * 60)  !!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
    time_start = time.time()
    state = ''
    while time.time() - time_start < timeout:
        time.sleep(timeout / 20.0)
        url = URL_BASE + '/wizard/{}/status/'.format(PANEL_ID)
        resp = requests.get(url, headers={'Authorization': 'Bearer ' + TOKEN})
        rc = common_check(resp)
        assert rc is True

        j_cont = resp.json()
        if len(j_cont) == 0:
            timeout /= 10.0
            continue
        try:
            for key, val in j_cont.iteritems():
                if val['config'].get('dect_id', None) is None:
                    continue
                if ipud_new not in val['config']['dect_id']['ipud']:
                    continue
                print 'state:', val['state']
                if val['state'] == 0:  # pending
                    state = 'Pending'
                elif val['state'] == 1:  # OK
                    state = 'OK'
                    print 'areas:', val['config']['areas']
                    print 'name:', val['config']['name']
                    print 'device_id:', val['config']['device_id']
                    print 'ts:', val['ts']
                elif val['state'] == 2:  # Failed
                    # rc = False
                    state = 'Failed'
                break

        except KeyError, e:
            print e

        if 'OK' in state or 'Failed' in state:
            break

    print 'Time elapsed {} sec.'.format(time.time() - time_start)
    return state


def status_delete():
    print '\n[' + sys._getframe().f_code.co_name + ']'
    url = URL_BASE + '/wizard/{}/status/'.format(PANEL_ID)
    resp = requests.delete(url, headers={'Authorization': 'Bearer ' + TOKEN})
    rc = common_check(resp)
    assert rc is True

    j_cont = resp.json()
    print j_cont
    assert j_cont['msg'] == 'ok'


##############################
# #         Params         # #
##############################

def device_params_get(device_id, refer_params):
    print '\n[' + sys._getframe().f_code.co_name + '] id:{}'.format(device_id)
    timeout = 60  # 1 min (10 * 60)
    time_start = time.time()
    while time.time() - time_start < timeout:
        time.sleep(timeout / 10.0)
        params = zones_zone_params_get(device_id)
        if len(params) == 0:
            continue

        # print 'gain_level:', params['gain_level']
        assert params['gain_level'] == refer_params['gain_level']
        # print 'pet_immuni:', params['pet_immuni']
        assert params['pet_immuni'] == refer_params['pet_immuni']
        break

    print 'Time elapsed {} sec.'.format(time.time() - time_start)


def main():
    # print DEVICE_TYPES
    print 'Hello', PANEL_ID


if __name__ == '__main__':
    main()
