from requests.exceptions import HTTPError

PANEL_ID = 24

# Zone Device types
SDEVTYPE_PIR = 0x31  # Infrared motion detector
SDEVTYPE_MAG = 0x32  # Magnetic contact
SDEVTYPE_RMT = 0x33  # Pendant remote control
SDEVTYPE_SMK = 0x34  # Smoke detector
SDEVTYPE_GAS = 0x35  # Gas detector
SDEVTYPE_GBD = 0x36  # Glass break detector
SDEVTYPE_CAM = 0x37  # Camera PIR
SDEVTYPE_FLD = 0x38  # Flood detector
SDEVTYPE_VIB = 0x39  # Vibration sensor
SDEVTYPE_HAM = 0x3A  # Home automation
SDEVTYPE_SPO = 0x3B  # Heart & saturation monitor
SDEVTYPE_TFM = 0x3C  # Temperature-flood-magnet detector
SDEVTYPE_SRN = 0x45  # Siren
SDEVTYPE_OUT = 0x57  # Output (relay board)
SDEVTYPE_KPD = 0x98  # Keypad
SDEVTYPE_SOC = 0xe7  # AC Socket

ZONE = 1
OUTPUT = 2

DEVICE_TYPES = {
    SDEVTYPE_PIR: ZONE,
    SDEVTYPE_MAG: ZONE,
    SDEVTYPE_RMT: ZONE,
    SDEVTYPE_SMK: ZONE,
    SDEVTYPE_GAS: ZONE,
    SDEVTYPE_GBD: ZONE,
    SDEVTYPE_CAM: ZONE,
    SDEVTYPE_FLD: ZONE,
    SDEVTYPE_VIB: ZONE,
    SDEVTYPE_HAM: OUTPUT,
    SDEVTYPE_SPO: ZONE,
    SDEVTYPE_TFM: ZONE,
    SDEVTYPE_SRN: OUTPUT,
    SDEVTYPE_OUT: ZONE,
    SDEVTYPE_KPD: ZONE,
    SDEVTYPE_SOC: OUTPUT,
}


def common_check(resp):
    status_code = resp.status_code
    print 'status code:', status_code
    try:
        resp.raise_for_status()
    except HTTPError, e:
        print e
        assert False

    assert status_code == 200

    if 'json' not in resp.headers['content-type']:
        print 'Non-json response'
        assert False

    return True

