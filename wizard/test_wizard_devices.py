import pytest
import time

from pytests.common_stuff import *

from pytests.url_functions import devices_get
from pytests.url_functions import devices_cam_post
from pytests.url_functions import devices_acplug_post
from pytests.url_functions import status_cam_get
from pytests.url_functions import status_acplug_get
from pytests.url_functions import status_delete
from pytests.url_functions import device_params_get

from pytests.url_functions import config_mode_post
from pytests.url_functions import config_mode_delete

from pytests.url_functions import zones_zone_delete

# # Zone Device types
# SDEVTYPE_PIR = 0x31  # Infrared motion detector
# SDEVTYPE_MAG = 0x32  # Magnetic contact
# SDEVTYPE_RMT = 0x33  # Pendant remote control
# SDEVTYPE_SMK = 0x34  # Smoke detector
# SDEVTYPE_GAS = 0x35  # Gas detector
# SDEVTYPE_GBD = 0x36  # Glass break detector
# SDEVTYPE_CAM = 0x37  # Camera PIR
# SDEVTYPE_FLD = 0x38  # Flood detector
# SDEVTYPE_VIB = 0x39  # Vibration sensor
# SDEVTYPE_HAM = 0x3A  # Home automation
# SDEVTYPE_SPO = 0x3B  # Heart & saturation monitor
# SDEVTYPE_TFM = 0x3C  # Temperature-flood-magnet detector
# SDEVTYPE_SRN = 0x45  # Siren
# SDEVTYPE_OUT = 0x57  # Output (relay board)
# SDEVTYPE_KPD = 0x98  # Keypad
# SDEVTYPE_SOC = 0xe7  # AC Socket
#
# ZONE = 1
# OUTPUT = 2
#
# DEVICE_TYPES = {
#     SDEVTYPE_PIR: ZONE,
#     SDEVTYPE_MAG: ZONE,
#     SDEVTYPE_RMT: ZONE,
#     SDEVTYPE_SMK: ZONE,
#     SDEVTYPE_GAS: ZONE,
#     SDEVTYPE_GBD: ZONE,
#     SDEVTYPE_CAM: ZONE,
#     SDEVTYPE_FLD: ZONE,
#     SDEVTYPE_VIB: ZONE,
#     SDEVTYPE_HAM: OUTPUT,
#     SDEVTYPE_SPO: ZONE,
#     SDEVTYPE_TFM: ZONE,
#     SDEVTYPE_SRN: OUTPUT,
#     SDEVTYPE_OUT: ZONE,
#     SDEVTYPE_KPD: ZONE,
#     SDEVTYPE_SOC: OUTPUT,
# }

DEVICE_IDS = []  # ???
DEVICES_ID2TYPE = {}
LAST_ADDED_ID = -1
CAM_WIZARD_PARAMS = {
    "name": "cam",
    "code": "0-R-37-00-900053",
    "roomSize": "medium",
    "hasPet": "1",
    "petSize": "medium",
    "furSize": "long",
    "cameraMode": "picture",
    "stayMode": "1"
}

ACPLUG_WIZARD_PARAMS = {
    "name": "AC plug",
    "code": "0-D-E7-00-027BA00D83",
    # "stayMode": "1"
}

CAM_REFER_PARAMS = {
    "gain_level": 3,
    "pet_immuni": True,
    "stay_mode_zone": True
}

AC_REFER_PARAMS = {
    "ipud": "02:7B:A0:0D:83"
}


# @pytest.mark.run(order=1)
def test_enter_installer_mode():
    config_mode_post()


# @pytest.mark.run(order=2)
def test_get_devices_list():
    global DEVICE_IDS
    global LAST_ADDED_ID
    devices_get(DEVICE_IDS)
    DEVICE_IDS.sort()
    # if len(DEVICE_IDS):
    #     LAST_ADDED_ID = DEVICE_IDS[-1]

    print 'DEVICES:', DEVICE_IDS, 'LAST:', LAST_ADDED_ID


# @pytest.mark.skip(reason='not relevant')
# @pytest.mark.run(order=3)
def test_add_device_cam():
    global DEVICE_IDS
    global LAST_ADDED_ID
    device_id = devices_cam_post(CAM_WIZARD_PARAMS, CAM_REFER_PARAMS)
    LAST_ADDED_ID = device_id
    DEVICE_IDS.append(device_id)
    DEVICE_IDS.sort()
    print 'DEVICES:', DEVICE_IDS, 'LAST:', LAST_ADDED_ID


@pytest.mark.skip(reason='not relevant')
# @pytest.mark.xfail
def test_add_same_device_cam():
    global DEVICE_IDS
    global LAST_ADDED_ID
    device_id = devices_cam_post(CAM_WIZARD_PARAMS, CAM_REFER_PARAMS)
    LAST_ADDED_ID = device_id
    DEVICE_IDS.append(device_id)
    DEVICE_IDS.sort()
    print 'DEVICES:', DEVICE_IDS, 'LAST:', LAST_ADDED_ID


# @pytest.mark.skip(reason='not relevant')
def test_get_device_cam_status():
    global DEVICE_IDS
    global LAST_ADDED_ID
    state = status_cam_get(CAM_WIZARD_PARAMS)
    if 'OK' not in state:
        if LAST_ADDED_ID > -1:
            DEVICE_IDS.remove(LAST_ADDED_ID)
            LAST_ADDED_ID = -1
            assert False


# @pytest.mark.skip(reason='not relevant')
def test_device_cam_params():
    if LAST_ADDED_ID > -1:
        device_params_get(LAST_ADDED_ID, CAM_REFER_PARAMS)


# @pytest.mark.skip(reason='not relevant')
def test_enter_installer_mode2():
    config_mode_post()


# @pytest.mark.skip(reason='not relevant')
# @pytest.mark.xfail
# @pytest.mark.run(order=4)
def test_delete_zone():
    global DEVICE_IDS
    global LAST_ADDED_ID
    if LAST_ADDED_ID > -1:
        zones_zone_delete(LAST_ADDED_ID)
        DEVICE_IDS.remove(LAST_ADDED_ID)
        LAST_ADDED_ID = -1
    else:
        print 'No zone to delete'
    print 'DEVICES:', DEVICE_IDS, 'LAST:', LAST_ADDED_ID


def test_delete_status():
    status_delete()


def test_add_device_acplug():
    global DEVICE_IDS
    global LAST_ADDED_ID
    device_id = devices_acplug_post(ACPLUG_WIZARD_PARAMS)
    LAST_ADDED_ID = device_id
    DEVICE_IDS.append(device_id)
    DEVICE_IDS.sort()
    print 'DEVICES:', DEVICE_IDS, 'LAST:', LAST_ADDED_ID


@pytest.mark.skip(reason='not relevant')
# @pytest.mark.xfail
def test_add_same_device_acplug():
    global DEVICE_IDS
    global LAST_ADDED_ID
    device_id = devices_acplug_post(ACPLUG_WIZARD_PARAMS)
    LAST_ADDED_ID = device_id
    DEVICE_IDS.append(device_id)
    DEVICE_IDS.sort()
    print 'DEVICES:', DEVICE_IDS, 'LAST:', LAST_ADDED_ID


@pytest.mark.skip(reason='not relevant')
def test_get_device_acplug_status():
    global DEVICE_IDS
    global LAST_ADDED_ID
    state = status_acplug_get(ACPLUG_WIZARD_PARAMS)
    if 'OK' not in state:
        if LAST_ADDED_ID > -1:
            DEVICE_IDS.remove(LAST_ADDED_ID)
            LAST_ADDED_ID = -1
            assert False


# @pytest.mark.skip(reason='not relevant')
def test_enter_installer_mode3():
    config_mode_post()


# # @pytest.mark.run(order=10)
def test_exit_installer_mode():
    config_mode_delete()


def test_print_devices():
    print 'DEVICES:', DEVICE_IDS
